# ttf-inter

Inter is a typeface specially designed for user interfaces with focus on high legibility of small-to-medium sized text on computer screens

https://github.com/rsms/inter

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/fonts/ttf-inter.git
```
